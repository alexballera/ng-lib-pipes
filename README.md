# Crear y Publicar librerías Angular en npm

## Crear librerías
### Generamos un espacio de trabajo con el ng cli

<br>

```console
ng new ng-workspace --create-application=false
cd ng-workspace
``` 
<img src="./01.png">

<br>

### Verificamos que el nombre de la librería este disponible en :
https://www.npmjs.com

Para este ejemplo: https://www.npmjs.com/package/demo-pipes

<img src="./02.png">

<br>
<br>
<br>
<br>


### Generamos la librería desde el ng cli

```console
ng g library demo-pipes
```
Esto genera una carpeta llamada projects/demo-pipes y actualiza varios archivos.

<img src="./03.png">

<br>

### Borramos los siguientes archivos que no usaremos
- demo-pipes.service.ts
- demo-pipes.service.spec.ts
- demo-pipes.component.spec.ts
- demo-pipes.component.ts


## Creación de Pipes

nota: Los pipes deben crearse manualmente

### Para este ejemplo se crearan 3 pipes
crearemos un directorio donde estaran los pipes

#### #1 ngCurrency 
Creamos el siguiente archivo ng-currency.pipe.ts en la carpeta projects/demo-pipes/lib/pipes y exportamos el modulo

```javascript
import { Pipe, PipeTransform, NgModule } from '@angular/core';


@Pipe({
  name: 'ngCurrency'
})

export class NgCurrencyPipe implements PipeTransform {

  transform(amount: number, decimals?: number, symbol?: string): string {
    decimals = decimals != null ? decimals : 2;
    symbol = symbol != null ? symbol : '$';

    return symbol + ' ' + amount
      .toFixed(decimals)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }

}

@NgModule({
  declarations: [NgCurrencyPipe],
  exports: [NgCurrencyPipe],
})
export class ngCurrencyModule {}

```


#### #2 ngUppercase
Creamos el siguiente archivo ng-uppercase.pipe.ts en la carpeta projects/demo-pipes/lib/pipes y exportamos el modulo

```javascript
import {NgModule, Pipe, PipeTransform} from '@angular/core';


@Pipe({
  name: 'ngUppercase'
})

export class NgUppercasePipe implements PipeTransform {

  transform(content: string) {

    return content.toUpperCase();
  }

}

@NgModule({
  declarations: [
    NgUppercasePipe
  ],
  exports: [
    NgUppercasePipe
  ]
})

export class NgUppercaseModule { }

```


</br>

#### #3 ngCard
Creamos el siguiente archivo ng-card.pipe.ts en la carpeta projects/demo-pipes/lib/pipes y exportamos el modulo

```js
import { Pipe, PipeTransform, NgModule } from '@angular/core';

@Pipe({
  name: 'ngCard'
})

export class NgCardPipe implements PipeTransform {

  transform(value: string): string {
    return value
      .replace(/\s+/g, '')
      .replace(/(\d{4})/g, '$1 ')
      .trim();
  }

}


@NgModule({
  declarations: [NgCardPipe],
  exports: [NgCardPipe],
})

export class NgCardModule {}

```

#### El modulo que va a importar y exportar los módulos
Creamos el siguiente archivo pipes.module.ts en la carpeta projects/demo-pipes/lib/pipes
```js

import { NgCurrencyModule } from './ng-currency.pipe';
import { NgModule } from '@angular/core';
import { NgCardModule } from './ng-card.pipe';
import { NgUppercaseModule } from './ng-uppercase.pipe';


@NgModule({
  imports: [
    NgCurrencyModule,
    NgUppercaseModule,
    NgCardModule
  ],
  exports: [
    NgCurrencyModule,
    NgUppercaseModule,
    NgCardModule
  ]
})
export class PipesModule { }

```
#### En el archivo demo-pipes.module.ts exportamos el pipes.module

```js
import { NgModule } from '@angular/core';
import { PipesModule } from './pipes/pipes.module';

@NgModule({
  exports: [
    PipesModule
  ]
})
export class DemoPipesModule { }

```

### En el archivo public-api.ts exportamos todos los archivos de la librería 

```js

export * from './lib/demo-pipes.module';

export * from './lib/pipes/pipes.module';
export * from './lib/pipes/ng-currency.pipe'
export * from './lib/pipes/ng-card.pipe'
export * from './lib/pipes/ng-uppercase.pipe'

```
### Desactivar el motor Ivy de la librería, en el archivo tsconfig.lib.json, así debe de quedar el archivo

```json
{
  "extends": "../../tsconfig.base.json",
  "compilerOptions": {
    "outDir": "../../out-tsc/lib",
    "target": "es2015",
    "declaration": true,
    "inlineSources": true,
    "types": [],
    "lib": [
      "dom",
      "es2018"
    ]
  },
  "angularCompilerOptions": {
    "skipTemplateCodegen": true,
    "strictMetadataEmit": true,
    "enableResourceInlining": true,
    "enableIvy": false,
  },
  "exclude": [
    "src/test.ts",
    "**/*.spec.ts"
  ]
}

```

### Volvemos a la raíz del proyecto y ejecutamos

```console
npm run build demo-pipes --prod 
```
<img src="./04.png">



## Publicar librerías

- Crear cuenta en npm https://www.npmjs.com
- Iniciar sesión en npm en la consola

```console
npm login
```

 Nos movemos a la carpeta dist/demo-pipes
```console
cd dist/demo-pipes  
```

Ejecutamos el comando
```console
npm publish 
```

<img src="./05.png">


### Librería publicada

<img src="./06.png">


# Uso

Creamos un nuevo proyecto angular 

```console
ng new demo-app
```

Nos movemos al proyecto e instalamos nuestra librería

```console
cd demo-app
npm i demo-pipes
```

Importamos la librería en app.module.ts

```js
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DemoPipesModule } from 'demo-pipes';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DemoPipesModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Editamos app.component.html

```html
<h1>Pipes</h1>

<h3>ngCurrency</h3>
{{1233 | ngCurrency}}

<h3>ngUppercase</h3>
 {{ 'leonard' | ngUppercase}}

<h3>ngCard</h3>
 {{ '5555555555555555' | ngCard}}

```

<img src="./07.png">
